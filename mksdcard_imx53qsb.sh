#! /bin/sh

#######################################
# Check input parameters
#######################################

SDCARD_DEV=$1

# Simplify the device to its minimal expression
SDCARD_DEV="$(dirname $SDCARD_DEV)/$(basename $SDCARD_DEV)"

if [ -z $SDCARD_DEV ]; then
	echo "\ERROR: Must set the destination device\n"
	echo "Syntax:  $0 [device]"
	echo "Example: $0 /dev/sdg\n"
	exit 1
fi

# Make sure we are not trying to write to the primary partition
if [ $SDCARD_DEV = "/dev/sda" ]; then
	echo "\nERROR: Forbidden to write to /dev/sda\n"
	echo "Syntax:  $0 [device]"
	echo "Example: $0 /dev/sdg\n"
	exit 1
fi

# Check that we are writing to a block device
if [ ! -b $SDCARD_DEV ]; then
	echo "\nERROR: $SDCARD_DEV is not a block device\n"
	exit 1
fi

# Display mounted volumes
echo "Mounted volumes:"
mount

# One last confirmation
echo -e "\n\nWARNING: ${SDCARD_DEV} will be entirely erased!"
echo "Are you sure you want to continue (\"y\" to continue) y/*"
read confirm
if [ $confirm != "y" ]; then
	exit 0
fi

#######################################
# Create Partition Table
#######################################

# Unmount all mounted partitions
sudo umount ${SDCARD_DEV}*

# Check if the device name is mmcblkX
if echo "${SDCARD_DEV}" | grep -q mmcblk; then
	PART_NAME=${SDCARD_DEV}p
else
	PART_NAME=${SDCARD_DEV}
fi

# Zero out Partition Table
sudo dd if=/dev/zero of=${SDCARD_DEV} bs=1024 count=1

# Extract the number of cylinders
SIZE=`sudo fdisk -l ${SDCARD_DEV} | grep Disk | awk '{print $5}'`
CYLINDERS=`echo $SIZE/8225280 | bc`

echo DISK SIZE - $SIZE bytes
echo CYLINDERS - $CYLINDERS

{
echo 4,127,0xb,*
echo 131,32,,-
echo 164,34,E
echo 199,
echo 165,28,L
echo 194,4,L
} | sudo sfdisk -H 255 -S 63 -C $CYLINDERS ${SDCARD_DEV}

# Format all partitions
echo "Formatting partitions..."
sudo dd if=/dev/zero of=${PART_NAME}1 bs=512 count=1
sudo mkfs.vfat -F 32 ${PART_NAME}1 -n sdcards	&& \
sudo mkfs.ext4 ${PART_NAME}2 -O ^extent -L system	&& \	
sudo mkfs.ext4 ${PART_NAME}4 -O ^extent -L recovery	&& \
sudo mkfs.ext4 ${PART_NAME}5 -O ^extent -L data	&& \
sudo mkfs.ext4 ${PART_NAME}6 -O ^extent -L cache

if [ $? -ne 0 ]; then
	exit 1
fi

#######################################
# Erase U-Boot environment
#######################################

echo "Erasing U-Boot environment..."

sudo dd if=/dev/zero of=${SDCARD_DEV} bs=1k seek=768 count=128 || exit 1
sync


#######################################
# Copy the bootloader
#######################################

echo "Copying u-boot..."

sudo dd if=../u-boot/u-boot.bin of=${SDCARD_DEV} bs=1k seek=1 skip=1 || exit 1
sync

